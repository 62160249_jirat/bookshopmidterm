/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.midtermexam;

import java.io.Serializable;

/**
 *
 * @author ACER
 */
public class Product implements Serializable {
    
    private int Id;
    private String Name;
    private String Brand;
    private double Price;
    private double Amount;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }
    
   Product(String Name, String Brand, double Price, double Amount) {
       this.Name = Name;
       this.Brand = Brand;
       this.Price = Price;
       this.Amount = Amount;
   }

    @Override
    public String toString() {
        return  "id :  " + Id + "  |  Name :  " + Name + "  |  Brand :  " + Brand + "  |  Price :  " 
                    + Price + "  |  Amount :  " + Amount ;
    }
    
   
}
