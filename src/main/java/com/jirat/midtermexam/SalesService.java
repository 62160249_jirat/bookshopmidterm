/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.midtermexam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class SalesService {
    private static ArrayList<Product> productList = new ArrayList<>();
    static{
        load();
    }
    
    //Create
    public static boolean addProduct(Product item) {
        productList.add(item);
        refreshID();
        save();
        return true;
    }
    
    //Delete
    public static boolean delProduct(Product item) {
        productList.remove(item);
        refreshID();
        save();
        return true;
    }
    
    public static boolean delProduct(int index) {
        productList.remove(index);
        refreshID();
        save();
        return true;
    }
    
    public static boolean delAllProduct() {
        productList.removeAll(productList);
        refreshID();
        save();
        return true;
    }
    
    //Read
    public static ArrayList<Product>getProduct(){
        refreshID();
        save();
        return productList;
    }
    
    public static Product getProduct(int index) {
        refreshID();
        save();
        return productList.get(index);
    }
    
    //Update
    public static boolean updateProduct(int index, Product item) {
        refreshID();
        save();
        return true;
    }
    
    //Total
    public static String totalPrice(){
        double sum = 0;
        for(int i = 0; i < productList.size(); i++){
            sum += productList.get(i).getPrice() * productList.get(i).getAmount();
        }
        return sum +"";
    }
    
    //Amount
    public static String totalAmount(){
        int sum1 = 0;
        for(int i = 0; i < productList.size(); i++) {
            sum1 += productList.get(i).getAmount();
            
        }
        return sum1 + "";
    }
    
    public static String refreshID() {
        if(!productList.isEmpty()) {
            int id = 0;
            for(int i = 0; i < productList.size(); i++) {
                productList.get(i).setId(i+1);
                id = i + 2;
            } 
            return Integer.toString(id);
        }
        return "1";
    }
    
    //Save File
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Book.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Load File
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Book.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}


